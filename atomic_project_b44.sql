-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2017 at 07:50 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b44`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_b44` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_b44`;

-- --------------------------------------------------------

--
-- Table structure for table `birth_date`
--

CREATE TABLE IF NOT EXISTS `birth_date` (
`id` int(11) NOT NULL,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birth_date`
--

INSERT INTO `birth_date` (`id`, `user_name`, `birth_date`, `soft_delete`) VALUES
(1, 'raihan', '2017-01-03', 'Yes'),
(2, 'Mohammad Arif', '2006-02-08', 'No'),
(3, 'Mohammad Rubel', '2017-01-02', 'No'),
(4, 'raihan', '2017-01-10', 'Yes'),
(5, 'Jahid Hasan Sakib', '2017-02-25', 'No'),
(6, 'Sohel Rana', '2017-02-06', 'No'),
(7, 'Nazim Uddin', '2017-02-06', 'No'),
(8, 'Sohel', '2017-02-06', 'Yes'),
(9, 'Sohel', '2017-02-15', 'No'),
(10, 'Jahid Hasan Sakib', '2017-02-24', 'No'),
(11, 'Mohammad Rubel', '2017-02-03', 'No'),
(12, 'Mohammad Arif', '2017-02-07', 'Yes'),
(13, 'Kaniz Fatema', '2015-05-29', 'No'),
(14, 'Sohel', '2017-02-08', 'Yes'),
(15, 'Mahmudul Hasan Sohag', '2017-02-14', 'Yes'),
(16, 'Abu Nayeem', '2017-02-22', 'No'),
(17, 'test data', '2017-02-25', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(1, 'Advanced Learners English Grammar', 'Chowdhury and Hossain', 'No'),
(2, 'The Beauty of Mathematics', 'S. M. Zakir Hossain', 'No'),
(3, 'Jogajog ', 'Robindranath Tagore', 'No'),
(4, 'A Passage to the English Language', 'S. M. Zakir Hossain', 'No'),
(5, 'Advanced Learners English Grammar', 'Chowdhury and Hossain', 'No'),
(6, 'BCS English ', 'Professor''sProkashon', 'Yes'),
(7, 'Satkahon', 'Somresh Majumder', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `city_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`, `soft_delete`) VALUES
(1, 'Dhaka', 'No'),
(2, 'Chittagong', 'No'),
(3, 'Rajshahi', 'No'),
(5, 'Rajshahi', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `email_address` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email_address`, `soft_delete`) VALUES
(1, 'abcdefghijkl@gmail.com', 'No'),
(2, 'rubelwd@gmail.com', 'No'),
(4, 'md.rubeliiuc@gmail.com', 'No'),
(5, 'jhsdajsjafnj@gmail.com', 'No'),
(6, 'abcdefgh@gmail.com', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `applicant_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `applicant_gender` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `applicant_name`, `applicant_gender`, `soft_delete`) VALUES
(1, 'Arif uddin', 'Male', 'No'),
(2, 'Rumi Akter', 'Female', 'Yes'),
(3, 'Sahed Iqbal', 'Male', 'No'),
(4, 'Nisso', 'Female', 'No'),
(5, 'Nayeem', 'Male', 'No'),
(6, 'Kalam', 'Male', 'No'),
(7, 'Sahed Iqbal', 'Male', 'No'),
(8, 'Sumi Akter', 'Female', 'No'),
(9, 'Sahed Iqbal', 'Male', 'No'),
(10, 'Sahed Iqbal', 'Male', 'No'),
(11, 'Sahed Iqbal', 'Male', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `student_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `student_name`, `hobbies`, `soft_delete`) VALUES
(3, 'Sahed Iqbal', 'Playing Guiter,Reading', 'No'),
(4, 'Jamal Uddin', 'Painting', 'Yes'),
(5, 'Sahed Sazzad', 'Gardenning', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `picture`, `soft_delete`) VALUES
(6, 'adfdfvg', 'Untitled.png', 'No'),
(7, 'Sohel Rana', 'Untitled.jpg', 'No'),
(8, 'sahed', 'Untitled2.png', 'Yes'),
(10, 'kamal', 'Untitled4.jpg', 'Yes'),
(12, 'New Image Upload for unlink', 'Desert.jpg', 'No'),
(13, 'new profile picture', 'Tulips.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summery_of_org`
--

CREATE TABLE IF NOT EXISTS `summery_of_org` (
`id` int(11) NOT NULL,
  `org_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `summery` text COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `summery_of_org`
--

INSERT INTO `summery_of_org` (`id`, `org_name`, `summery`, `soft_delete`) VALUES
(1, 'Sanmar', 'Checkboxes are for selecting one or several options in a list, while radios are for selecting one option from many.', 'No'),
(2, 'Building Technology and Ideas', 'Disabled checkboxes and radios are supported, but to provide a "not-allowed" cursor on hover of the parentDisabled checkboxes and radios are supported, but to provide a "not-allowed" cursor on hover of the parentDisabled checkboxes and radios are supported, but to provide a "not-allowed" cursor on hover of the parentDisabled checkboxes and radios are supported, but to provide a "not-allowed" cursor on hover of the parent', 'Yes'),
(3, 'CPDL', 'Welcome to Sample CMS Blog! Weâ€™ve assembled some links to get you started: Welcome to Sample CMS Blog! Weâ€™ve assembled some links to get you started:Welcome to Sample CMS Blog! Weâ€™ve assembled some links to get you started:Welcome to Sample CMS Blog! Weâ€™ve assembled some links to get you started:Welcome to Sample CMS Blog! Weâ€™ve assembled some links to get you started:Welcome to Sample CMS Blog! Weâ€™ve assembled some links to get you started:', 'No'),
(4, 'Sanmar', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Yes'),
(5, 'Bangladesh Telecommunication and Telegraph Board', 'This is a government organization', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_date`
--
ALTER TABLE `birth_date`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summery_of_org`
--
ALTER TABLE `summery_of_org`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_date`
--
ALTER TABLE `birth_date`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `summery_of_org`
--
ALTER TABLE `summery_of_org`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
